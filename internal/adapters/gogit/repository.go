package gogit

import (
	"fmt"
	"os"
	"path/filepath"
	"time"

	git "gopkg.in/src-d/go-git.v4"
)

func CreateRepository(repoPath, hookPath string) error {
	if _, err := git.PlainInit(repoPath, true); err != nil {
		return err
	} else {
		return linkHook(repoPath, hookPath)
	}
}

func linkHook(repoPath, hookPath string) error {
	repoHookPath := filepath.Join(repoPath, "hooks")
	realLocalHookPath, _ := filepath.EvalSymlinks(repoHookPath)
	realHookPath, _ := filepath.EvalSymlinks(hookPath)
	if realLocalHookPath != realHookPath {
		if _, err := os.Stat(repoHookPath); err == nil {
			os.Rename(repoHookPath, fmt.Sprintf("%s.old.%d", repoHookPath, time.Now().Unix()))
		}
		return os.Symlink(hookPath, repoHookPath)
	}
	return nil
}
